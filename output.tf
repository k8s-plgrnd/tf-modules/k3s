output "kubeconfig" {
  value = replace(
    data.external.kubeconfig.result.kubeconfig,
    "server: https://127.0.0.1:6443",
    "server: https://${local.master_public_ip}:6443"
  )
}
