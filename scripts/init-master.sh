#!/bin/sh
cat <<EOF > /etc/systemd/system/k3s-server.service
[Unit]
Description=Lightweight Kubernetes
Documentation=https://k3s.io
After=network-online.target

[Install]
WantedBy=multi-user.target

[Service]
Type=notify
EnvironmentFile=/etc/systemd/system/k3s-server.env
KillMode=process
Delegate=yes
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
TimeoutStartSec=0
Restart=always
RestartSec=5s
ExecStart=/usr/local/bin/k3s server \
  --flannel-backend wireguard \
  --node-taint node-role.kubernetes.io/master:NoSchedule \
  --node-taint CriticalAddonsOnly:NoSchedule \
  --node-ip ${master_ip} \
  --node-external-ip ${public_ip} \
  --advertise-address ${master_ip} ${extra_flags}
EOF

cat << EOF > /etc/systemd/system/k3s-server.env
K3S_TOKEN="${cluster_token}"
EOF

ufw allow in on cni0 from 10.42.0.0/16 comment 'Pod to pod communication'
ufw allow from ${node_network} to ${master_ip} port 6443 comment 'Kubernetes nodes to apiserver'
ufw allow from ${node_network} to ${master_ip} port 10250 comment 'Metrics-server'

if [ "${public_apiserver}" = "true" ]; then
	ufw allow to any port 6443 comment 'Public apiserver'
fi

systemctl daemon-reload
systemctl enable k3s-server
systemctl start k3s-server
