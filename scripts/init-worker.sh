#!/bin/sh
cat <<EOF > /etc/systemd/system/k3s-agent.service
[Unit]
Description=Lightweight Kubernetes
Documentation=https://k3s.io
After=network-online.target

[Install]
WantedBy=multi-user.target

[Service]
Type=notify
EnvironmentFile=/etc/systemd/system/k3s-agent.env
KillMode=process
Delegate=yes
# Having non-zero Limit*s causes performance problems due to accounting overhead
# in the kernel. We recommend using cgroups to do container-local accounting.
LimitNOFILE=1048576
LimitNPROC=infinity
LimitCORE=infinity
TasksMax=infinity
TimeoutStartSec=0
Restart=always
RestartSec=5s
ExecStart=/usr/local/bin/k3s agent \
  --node-ip ${private_ip} \
  --node-external-ip ${public_ip} ${extra_flags}
EOF

cat << EOF > /etc/systemd/system/k3s-agent.env
K3S_TOKEN="${cluster_token}"
K3S_URL="https://${master_ip}:6443"
EOF

ufw allow in on cni0 from 10.42.0.0/16 comment 'Pod to pod communication'
ufw allow from ${node_network} to ${private_ip} port 10250 comment 'Metrics-server'

systemctl daemon-reload
systemctl enable k3s-agent
systemctl start k3s-agent
