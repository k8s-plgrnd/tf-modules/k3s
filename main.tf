provider "random" {
  version = "~> 2.2"
}

resource "random_string" "token" {
  length  = 64
  special = false
}

locals {
  master_ip        = var.masters[0].private_ip
  master_public_ip = var.masters[0].public_ip
  cluster_token    = random_string.token.result
}

resource "null_resource" "init-master" {
  connection {
    host  = local.master_public_ip
    user  = "root"
    agent = true
  }

  provisioner "remote-exec" {
    inline = [templatefile("${path.module}/scripts/init-master.sh", {
      cluster_token    = local.cluster_token
      master_ip        = local.master_ip
      public_ip        = local.master_public_ip
      extra_flags      = var.master_extra_flags
      node_network     = var.node_network
      public_apiserver = var.public_apiserver
    })]
  }
}

resource "null_resource" "init-workers" {
  count = length(var.workers)

  connection {
    host  = var.workers[count.index].public_ip
    user  = "root"
    agent = true
  }

  provisioner "remote-exec" {
    inline = [templatefile("${path.module}/scripts/init-worker.sh", {
      master_ip     = local.master_ip
      cluster_token = local.cluster_token
      private_ip    = var.workers[count.index].private_ip
      public_ip     = var.workers[count.index].public_ip
      node_network  = var.node_network
      extra_flags   = var.worker_extra_flags
    })]
  }
}

data "external" "kubeconfig" {
  depends_on = [null_resource.init-master]

  program = [
    "sh",
    "-c",
    "jq -n --arg kubeconfig \"$(ssh -o 'UserKnownHostsFile=/dev/null' -o 'StrictHostKeyChecking=no' root@${local.master_public_ip} cat /etc/rancher/k3s/k3s.yaml)\" '{$kubeconfig}'",
  ]
}
