variable "masters" {
  type = list(object({
    id         = string
    public_ip  = string
    private_ip = string
  }))
  default = []
}

variable "workers" {
  type = list(object({
    id         = string
    public_ip  = string
    private_ip = string
  }))
  default = []
}

variable "public_apiserver" {
  type    = bool
  default = false
}
variable "node_network" { # For ufw rules
  type = string
}

variable "master_extra_flags" {
  type    = string
  default = ""
}

variable "worker_extra_flags" {
  type    = string
  default = ""
}
